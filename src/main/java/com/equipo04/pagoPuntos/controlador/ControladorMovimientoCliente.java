package com.equipo04.pagoPuntos.controlador;

import com.equipo04.pagoPuntos.modelo.Movimiento;
import com.equipo04.pagoPuntos.modelo.MovimientosCliente;
import com.equipo04.pagoPuntos.servicio.ServicioMovimientoClientes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(Rutas.CLIENTES + "/{idInterno}/movimientos")
public class ControladorMovimientoCliente {

    @Autowired
    ServicioMovimientoClientes servicioMovimientoClientes;

    // POST http://localhost:9001/api/v1/clientes/{idInterno}/movimientos + DATOS -> agregarUnCanjeCliente(idInterno,cajes)
    @PostMapping
    public ResponseEntity agregarUnMovimientoCliente(@PathVariable String idInterno,
                                                @RequestBody Movimiento movimiento) {
        try {
            this.servicioMovimientoClientes.agregarMovimientoCliente(idInterno, movimiento);
        } catch(RuntimeException x) {
            System.err.println(x);
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().build();
    }

    // GET http://localhost:9001/api/v1/clientes/{idInterno}/movimientos -> obtenerMovimientosUnCliente(idInterno)
    @GetMapping
    public ResponseEntity<MovimientosCliente> obtenerMovimientosUnCliente(@PathVariable String idInterno) {
        try {
            final var movimientosCliente = this.servicioMovimientoClientes.obtenerMovimientosUnCliente(idInterno);
            return ResponseEntity.ok(movimientosCliente);
        } catch(RuntimeException x) {
            return ResponseEntity.notFound().build();
        }
    }
}
