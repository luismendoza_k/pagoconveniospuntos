package com.equipo04.pagoPuntos.controlador;

import com.equipo04.pagoPuntos.modelo.Convenio;
import com.equipo04.pagoPuntos.servicio.ServicioConvenio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;


@RestController
@RequestMapping(com.equipo04.pagoPuntos.util.Rutas.CONVENIO)
public class ControladorConvenio {

    @Autowired
    ServicioConvenio servicioConvenio;
    @Autowired
    PagedResourcesAssembler pagedResourcesAssembler;

    @PostMapping
    public ResponseEntity insertarConvenio(@RequestBody Convenio convenio) {
        this.servicioConvenio.insertarConvenio(convenio);

        final Link enlaceEsteDocumento = linkTo(methodOn(ControladorConvenio.class).obtenerConvenio(convenio.codConvenio)).withSelfRel();
        return ResponseEntity.ok().location(enlaceEsteDocumento.toUri()).build();
    }


    @GetMapping("/{codConvenio}")
    public ResponseEntity<Convenio> obtenerConvenio(@PathVariable String codConvenio) {
        try {
            return ResponseEntity.ok(this.servicioConvenio.obtenerConvenio(codConvenio));
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping
    public PagedModel<EntityModel<Convenio>> listarConvenios(@RequestParam int pagina, @RequestParam int cantidad) {
        final var page = this.servicioConvenio.listarConvenios(pagina, cantidad);
        return this.pagedResourcesAssembler.toModel(page);
        //return  ResponseEntity.ok().body(this.servicioConvenio.listarConvenios(pagina, cantidad));
    }

    @PutMapping("/{codConvenio}")
    public ResponseEntity reemplazarConvenio(@PathVariable String codConvenio, @RequestBody Convenio convenio) {
        try {

            this.servicioConvenio.actualizarConvenio(codConvenio, convenio);

        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().build();
    }

    @PatchMapping("/{codConvenio}")
    public ResponseEntity actualizarCliente(@PathVariable String codConvenio, @RequestBody Convenio convenio) {
        try {
            this.servicioConvenio.emparcharConvenio(codConvenio, convenio);
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{codConvenio}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void eliminarConvenio(@PathVariable String codConvenio) {
        try {
            this.servicioConvenio.borrarConvenio(codConvenio);
        } catch (Exception e) {

        }
    }
}