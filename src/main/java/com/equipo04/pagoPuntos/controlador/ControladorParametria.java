package com.equipo04.pagoPuntos.controlador;

import com.equipo04.pagoPuntos.modelo.Convenio;
import com.equipo04.pagoPuntos.modelo.Parametria;
import com.equipo04.pagoPuntos.servicio.ServicioParametria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping(com.equipo04.pagoPuntos.util.Rutas.PARAMETRIA)
public class ControladorParametria {

    @Autowired
    ServicioParametria servicioParametria;
    @Autowired
    PagedResourcesAssembler pagedResourcesAssembler;

    @PostMapping
    public ResponseEntity insertarParametria(@RequestBody Parametria parametria){
        this.servicioParametria.insertarParametria(parametria);

        final Link enlaceEsteDocumento = linkTo(methodOn(ControladorParametria.class).obtenerParametria(parametria.codigo)).withSelfRel();
        return ResponseEntity.ok().location(enlaceEsteDocumento.toUri()).build();
    }


    @GetMapping("/{codigo}")
    public ResponseEntity<Parametria> obtenerParametria(@PathVariable String codigo){
        try{
            return  ResponseEntity.ok(this.servicioParametria.obtenerParametria(codigo));
        }catch (Exception e){
            return  ResponseEntity.notFound().build();
        }
    }

    @GetMapping
    public PagedModel<EntityModel<Convenio>> listarParametria(@RequestParam int pagina, @RequestParam int cantidad){
        final var page = this.servicioParametria.listarParametria(pagina, cantidad);
        return this.pagedResourcesAssembler.toModel(page);
        //return  ResponseEntity.ok().body(this.servicioConvenio.listarConvenios(pagina, cantidad));
    }

    @PutMapping("/{codigo}")
    public ResponseEntity reemplazarParametria(@PathVariable String codigo,@RequestBody Parametria parametria){
        try{

            this.servicioParametria.actualizarParametria(codigo,parametria);

        }catch (Exception e){
            return ResponseEntity.notFound().build();
        }
        return  ResponseEntity.ok().build();
    }

    @PatchMapping("/{codigo}")
    public ResponseEntity actualizarParametria(@PathVariable String codigo, @RequestBody Parametria parametria){
        try {
            this.servicioParametria.emparcharParametria(codigo,parametria);
        }catch (Exception e){
            return  ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok().build();
    }
    @DeleteMapping("/{codigo}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void eliminarParametria(@PathVariable String codigo){
        try {
            this.servicioParametria.borrarParametria(codigo);
        }catch (Exception e){

        }
    }



}