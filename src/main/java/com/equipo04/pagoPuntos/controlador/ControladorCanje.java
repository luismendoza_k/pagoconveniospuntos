package com.equipo04.pagoPuntos.controlador;

import com.equipo04.pagoPuntos.modelo.Canje;
import com.equipo04.pagoPuntos.modelo.Cliente;
import com.equipo04.pagoPuntos.servicio.ServicioCanje;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(Rutas.CANJES)
public class ControladorCanje {

    @Autowired
    ServicioCanje servicioCanje;

    @GetMapping
    public List<Canje> obtenerCanje(){
        return this.servicioCanje.obtenerCanje();
    }

    /*// POST http://localhost:9001/api/v1/canjes + DATOS -> crearCliente(Cliente)
    @PostMapping("/{idInterno}")
    public void crearCanje(@RequestBody Canje canje, @PathVariable String idInterno) {
        this.servicioCanje.insertarCanje(canje);
    }*/



}
