package com.equipo04.pagoPuntos.controlador;

import com.equipo04.pagoPuntos.modelo.Canje;
import com.equipo04.pagoPuntos.modelo.CanjesCliente;
import com.equipo04.pagoPuntos.servicio.ServicioCliente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(Rutas.CLIENTES + "/{idInterno}/canjes")
public class ControladorCanjeCliente {

    @Autowired
    ServicioCliente servicioCliente;

    // POST http://localhost:9001/api/v1/clientes/{idInterno}/canjes + DATOS -> agregarUnCanjeCliente(idInterno,cajes)
    @PostMapping
    public ResponseEntity agregarUnCanjeCliente(@PathVariable String idInterno,
                                                @RequestBody Canje canje) {
        try {
            this.servicioCliente.agregarCanjeCliente(idInterno, canje);


        } catch(RuntimeException x) {
            System.err.println(x);
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().build();
    }

    // GET http://localhost:9001/api/v1/clientes/{idInterno}/canjes -> obtenerCuentasCliente(documento)
    @GetMapping
    public ResponseEntity<CanjesCliente> obtenerCanjesUnCliente(@PathVariable String idInterno) {
        try {
            final var canjesCliente = this.servicioCliente.obtenerCanjesUnCliente(idInterno);
            return ResponseEntity.ok(canjesCliente);
        } catch(RuntimeException x) {
            return ResponseEntity.notFound().build();
        }
    }

}
