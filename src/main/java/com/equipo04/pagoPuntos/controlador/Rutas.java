package com.equipo04.pagoPuntos.controlador;

import com.equipo04.pagoPuntos.modelo.Convenio;
import com.equipo04.pagoPuntos.modelo.Parametria;
import com.equipo04.pagoPuntos.servicio.ServicioConvenio;
import com.equipo04.pagoPuntos.servicio.ServicioParametria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

public class Rutas {
    public static final String BASE = "/api/v1";
    public static final String CLIENTES = BASE + "/clientes";
    public static final String CANJES = BASE + "/canjes";

}
