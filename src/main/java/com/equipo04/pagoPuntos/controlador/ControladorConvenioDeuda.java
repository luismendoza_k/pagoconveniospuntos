package com.equipo04.pagoPuntos.controlador;

import com.equipo04.pagoPuntos.modelo.DeudaConvenio;
import com.equipo04.pagoPuntos.servicio.ServicioConvenio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(com.equipo04.pagoPuntos.util.Rutas.CONVENIO+ "/{codConvenio}/deudas" )
public class ControladorConvenioDeuda {

    @Autowired
    ServicioConvenio servicioConvenio;

    // POST http://localhost:9001/api/v1/convenios/{codConvenio}/deudas + DATOS -> agregarDeudaAConvenio(codConvenio,deuda)
    @PostMapping
    public ResponseEntity agregarDeudaAConvenio(@PathVariable String codConvenio,
                                                @RequestBody DeudaConvenio deuda) {
        try {
            this.servicioConvenio.agergarDeudaAConvenio(codConvenio, deuda);
        } catch(RuntimeException x) {
            System.err.println(x);
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().build();
    }

    // GET http://localhost:9001/api/v1/convenios/{codConvenio}/deudas
    @GetMapping
    public ResponseEntity ObtenerDeudasConvenio(@PathVariable String codConvenio) {
        try {

            return ResponseEntity.ok().body(this.servicioConvenio.obtenerDeudasConvenio(codConvenio));
        } catch(RuntimeException x) {
            return ResponseEntity.notFound().build();
        }

    }
}
