package com.equipo04.pagoPuntos.controlador;

import com.equipo04.pagoPuntos.modelo.Cliente;
import com.equipo04.pagoPuntos.modelo.Convenio;
import com.equipo04.pagoPuntos.modelo.DeudaConvenio;
import com.equipo04.pagoPuntos.servicio.ServicioCliente;
import com.equipo04.pagoPuntos.servicio.ServicioConvenio;
import com.equipo04.pagoPuntos.servicio.ServicioDeudaConvenio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping(Rutas.CLIENTES)
public class ControladorCliente {

    @Autowired
    ServicioCliente servicioCliente;


    // GET http://localhost:9001/api/v1/clientes?pagina=7&cantidad=3 -> List<Cliente> obtenerClientes()
    @GetMapping
    public List<Cliente> obtenerClientes(@RequestParam int pagina, @RequestParam int cantidad) {
        try {
            return this.servicioCliente.obtenerClientes(pagina -1 , cantidad);
        } catch (Exception x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    // GET http://localhost:9001/api/v1/clientes/{idInterno} -> obtenerUnCliente(idInterno)
    @GetMapping("/{idInterno}")
    public Cliente obtenerUnCliente(@PathVariable String idInterno){
        try {
            return this.servicioCliente.obtenerUnCliente(idInterno);
        } catch(Exception x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    // POST http://localhost:9001/api/v1/clientes + DATOS -> crearCliente(Cliente)
    @PostMapping
    public void crearCliente(@RequestBody Cliente cliente) {
        this.servicioCliente.insertarClienteNuevo(cliente);
    }

    // PUT http://localhost:9001/api/v1/clientes/12345678 + DATOS -> reemplazarUnCliente("12345678", DATOS)
    @PutMapping("/{idInterno}")
    public void actualizarUnCliente(@PathVariable("idInterno") String idInterno, @RequestBody Cliente cliente) {
        try {
            cliente.idInterno = idInterno;

            this.servicioCliente.actualizarCliente(cliente);
        } catch(Exception x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    // DELETE http://localhost:9001/api/v1/clientes/{idInterno} + DATOS -> eliminarUnCliente("{idInterno}")
    @DeleteMapping("/{idInterno}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void eliminarUnCliente(@PathVariable String idInterno) {
        try {
            this.servicioCliente.borrarCliente(idInterno);
        } catch (Exception e) {
        }


    }
}
