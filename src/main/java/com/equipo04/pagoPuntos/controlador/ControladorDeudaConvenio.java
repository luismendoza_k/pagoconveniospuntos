package com.equipo04.pagoPuntos.controlador;

import com.equipo04.pagoPuntos.modelo.Convenio;
import com.equipo04.pagoPuntos.modelo.DeudaConvenio;
import com.equipo04.pagoPuntos.servicio.ServicioDeudaConvenio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping(com.equipo04.pagoPuntos.util.Rutas.DEUDACONVENIO)
public class ControladorDeudaConvenio {

    @Autowired
    ServicioDeudaConvenio servicioDeudaConvenio;
    @Autowired
    PagedResourcesAssembler pagedResourcesAssembler;

    @PostMapping("/{codConvenio}")
    public ResponseEntity insertarDeudaConvenio(@PathVariable String codConvenio,
                                                @RequestBody DeudaConvenio deudaConvenio){
        this.servicioDeudaConvenio.insertarDeudaConvenio(codConvenio, deudaConvenio);

        final Link enlaceEsteDocumento = linkTo(methodOn(ControladorDeudaConvenio.class).obtenerDeudaConvenio(deudaConvenio.codExterno)).withSelfRel();
        return ResponseEntity.ok().location(enlaceEsteDocumento.toUri()).build();
    }

    @GetMapping("/{codExterno}")
    public ResponseEntity<DeudaConvenio> obtenerDeudaConvenio(@PathVariable String codExterno){
        try{
            return  ResponseEntity.ok(this.servicioDeudaConvenio.obtenerDeudaConvenio(codExterno));
        }catch (Exception e){
            return  ResponseEntity.notFound().build();
        }
    }

    @GetMapping
    public PagedModel<EntityModel<Convenio>> listarDeudaConvenios(@RequestParam int pagina, @RequestParam int cantidad){
        final var page = this.servicioDeudaConvenio.listarDeudaConvenios(pagina, cantidad);
        return this.pagedResourcesAssembler.toModel(page);
    }

    @PutMapping("/{codExterno}")
    public ResponseEntity reemplazarDeudaConvenio(@PathVariable String codExterno,@RequestBody DeudaConvenio deudaConvenio){
        try{

            this.servicioDeudaConvenio.actualizarDeudaConvenio(codExterno,deudaConvenio);

        }catch (Exception e){
            return ResponseEntity.notFound().build();
        }
        return  ResponseEntity.ok().build();
    }

    @DeleteMapping("/{codExterno}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void eliminarDeudaConvenio(@PathVariable String codExterno){
        try {
            this.servicioDeudaConvenio.borrarDeudaConvenio(codExterno);
        }catch (Exception e){

        }
    }
}
