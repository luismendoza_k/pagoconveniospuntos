package com.equipo04.pagoPuntos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PagoPuntosApplication {

	public static void main(String[] args) {
		SpringApplication.run(PagoPuntosApplication.class, args);
		System.out.println("*******************************");
		System.out.println(" APLICACION INICIADA ");
		System.out.println("*******************************");
	}

}
