package com.equipo04.pagoPuntos.util;

public class Rutas {
    public static final String BASE = "/api/v1";
    public static final String CONVENIO = BASE + "/convenios";
    public static final String DEUDACONVENIO = BASE + "/deudaconvenios";
    public static final String PARAMETRIA = BASE + "/parametrias";
}
