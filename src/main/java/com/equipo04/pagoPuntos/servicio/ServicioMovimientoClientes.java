package com.equipo04.pagoPuntos.servicio;

import com.equipo04.pagoPuntos.modelo.Movimiento;
import com.equipo04.pagoPuntos.modelo.MovimientosCliente;

public interface ServicioMovimientoClientes {

    public void agregarMovimientoCliente(String idInterno, Movimiento movimiento);

    public MovimientosCliente obtenerMovimientosUnCliente(String idInterno);
}
