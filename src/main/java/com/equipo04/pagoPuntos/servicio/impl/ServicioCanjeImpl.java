package com.equipo04.pagoPuntos.servicio.impl;

import com.equipo04.pagoPuntos.modelo.Canje;
import com.equipo04.pagoPuntos.servicio.ServicioCanje;
import com.equipo04.pagoPuntos.servicio.repositorio.RepositorioCanje;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ServicioCanjeImpl implements ServicioCanje {

    @Autowired
    RepositorioCanje repositorioCanje;

    @Override
    public List<Canje> obtenerCanje() {
        return this.repositorioCanje.findAll();
    }



    /*@Override
    public void insertarCanje(Canje canje) {
        this.repositorioCanje.insert(canje);
      //  System.err.println(String.format("==== insertarClienteNuevo %s = %s ","idInterno:", cliente.idInterno));
      //  System.err.println(String.format("==== insertarClienteNuevo %s = %s ","documento:", cliente.documento));
      //  System.err.println(String.format("==== insertarClienteNuevo %s = %s ","saldoPuntos:", cliente.saldoPuntos));
      //  System.err.println("************************************************");
    }*/
}
