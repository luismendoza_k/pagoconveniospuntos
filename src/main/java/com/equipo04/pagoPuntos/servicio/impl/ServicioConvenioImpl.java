package com.equipo04.pagoPuntos.servicio.impl;

import com.equipo04.pagoPuntos.modelo.Convenio;
import com.equipo04.pagoPuntos.modelo.DeudaConvenio;
import com.equipo04.pagoPuntos.servicio.ServicioConvenio;
import com.equipo04.pagoPuntos.servicio.repositorio.RepositorioConvenio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ServicioConvenioImpl implements ServicioConvenio {

    @Autowired
    RepositorioConvenio repositorioConvenio;

    @Override
    public void insertarConvenio(Convenio convenio) {
        this.repositorioConvenio.insert(convenio);
    }

    @Override
    public Convenio obtenerConvenio(String codConvenio) {
        final Optional<Convenio> quizasConvenio = this.repositorioConvenio.findById(codConvenio);
        return quizasConvenio.isPresent()? quizasConvenio.get():null;
    }

    @Override
    public Page<Convenio> listarConvenios(int pagina, int cantidad) {
        return this.repositorioConvenio.findAll(PageRequest.of(pagina,cantidad));
    }

    @Override
    public void actualizarConvenio( String  codConvenio,Convenio convenio) {
        convenio.codConvenio = codConvenio;
        this.repositorioConvenio.save(convenio);
    }

    @Override
    public void emparcharConvenio(String codConvenio,Convenio convenio) {
        convenio.codConvenio = codConvenio;
        final Convenio miConvenio = this.obtenerConvenio(codConvenio);

        if (miConvenio != null){
            if(convenio.desConvenio!=null){
                miConvenio.desConvenio= convenio.desConvenio;
            }
            if(convenio.estado!=null){
                miConvenio.estado=convenio.estado;
            }
            if(convenio.tipo!=null){
                miConvenio.tipo=convenio.tipo;
            }

            this.actualizarConvenio(codConvenio,miConvenio);
        }else{
            throw new RuntimeException("No existe el convenio: " + codConvenio);
        }


    }

    @Override
    public void borrarConvenio(String codConvenio) {
        this.repositorioConvenio.deleteById(codConvenio);
    }

    @Override
    public void agergarDeudaAConvenio(String codConvenio, DeudaConvenio deuda){
        final Convenio convenio = this.obtenerConvenio(codConvenio);
        if(!convenio.listaDeudas.contains(deuda.codExterno)){
            convenio.listaDeudas.add(deuda.codExterno);
            this.repositorioConvenio.save(convenio);
        }else{
            System.err.println("La deuda ya esta asociada");
        }

    }

    @Override
    public List<String> obtenerDeudasConvenio(String codConvenio){
        final Convenio convenio = this.obtenerConvenio(codConvenio);
        return convenio.listaDeudas;
    }
}
