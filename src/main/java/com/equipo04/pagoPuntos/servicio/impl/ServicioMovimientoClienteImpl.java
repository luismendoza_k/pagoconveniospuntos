package com.equipo04.pagoPuntos.servicio.impl;

import com.equipo04.pagoPuntos.modelo.*;
import com.equipo04.pagoPuntos.servicio.ServicioCliente;
import com.equipo04.pagoPuntos.servicio.ServicioDeudaConvenio;
import com.equipo04.pagoPuntos.servicio.ServicioMovimientoClientes;
import com.equipo04.pagoPuntos.servicio.repositorio.RepositorioCliente;
import com.equipo04.pagoPuntos.servicio.repositorio.RepositorioMovimiento;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.stereotype.Service;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.*;
import static org.springframework.data.mongodb.core.query.Criteria.where;

@Service
public class ServicioMovimientoClienteImpl implements ServicioMovimientoClientes {

    @Autowired
    RepositorioCliente repositorioCliente;

    @Autowired
    ServicioCliente servicioCliente;

    @Autowired
    RepositorioMovimiento repositorioMovimiento;

    @Autowired
    ServicioDeudaConvenio servicioDeudaConvenio;

    @Override
    public void agregarMovimientoCliente(String idInterno, Movimiento movimiento) {
        final Cliente cliente = this.servicioCliente.obtenerUnCliente(idInterno);
        final DeudaConvenio deudaConvenio = this.servicioDeudaConvenio.obtenerDeudaConvenio(movimiento.codExterno);
        final Canje canje = new Canje();

        canje.nroAutorizacion = movimiento.nroAutorizacion;
        canje.puntosCanjeados = deudaConvenio.puntosEquivalentes;
        canje.importeCanje = deudaConvenio.importe;
        canje.moneda = deudaConvenio.moneda;
        canje.estado = "Pendiente";
        canje.fechaCanje = movimiento.fecha;
        canje.nroMovimiento = movimiento.nroMovimiento;

        if (cliente.deudasConvenios.contains(movimiento.codExterno))
            throw new RuntimeException("Deuda pagada " + movimiento.codExterno);

        cliente.deudasConvenios.add(movimiento.codExterno);
        cliente.codigosCanjes.add(canje.nroAutorizacion);

        this.repositorioMovimiento.insert(movimiento);
        this.servicioCliente.agregarCanjeCliente(idInterno, canje);
        this.servicioCliente.actualizarCliente(cliente);

        //Validar que el Canje esta aprobado para recien grabar el movimiento.
        //Validar si el TipoPago es Puntos se tiene que guardar el Canje.
        //Validar que el Canje esta ok para recien actualizar datos en el Cliente
        //Validar que el Insert este ok para recien actualizar datos en el Cliente

    }

    @Autowired
    MongoOperations mongoOperations;

    @Override
    public MovimientosCliente obtenerMovimientosUnCliente(String idInterno) {
        final var r =
                this.mongoOperations.aggregate(newAggregation(
                        /* 1 */ match(where("_id").is(idInterno)),
                        /* 2 */ lookup("movimiento","deudasConvenios","_id","lstDeudas"),
                        /* 3 */ addFields().addFieldWithValue("idInterno", "$_id").build(),
                        /* 4 */ project("idInterno", "documento", "saldoPuntos", "lstDeudas")
                ), Cliente.class, MovimientosCliente.class);
        final var movimientosClientes = r.getMappedResults();
        if (movimientosClientes.size() > 0)
            return movimientosClientes.get(0);
        // Si no hay resultados -> objeto vacio.
        final var vacio = new MovimientosCliente();
        vacio.idInterno = idInterno;
        return vacio;
    }
}
