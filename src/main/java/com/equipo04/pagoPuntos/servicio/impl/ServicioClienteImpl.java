package com.equipo04.pagoPuntos.servicio.impl;

import com.equipo04.pagoPuntos.modelo.Canje;
import com.equipo04.pagoPuntos.modelo.CanjesCliente;
import com.equipo04.pagoPuntos.modelo.Cliente;

import com.equipo04.pagoPuntos.servicio.ServicioCliente;
import com.equipo04.pagoPuntos.servicio.repositorio.RepositorioCanje;
import com.equipo04.pagoPuntos.servicio.repositorio.RepositorioCliente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.*;
import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

@Service
public class ServicioClienteImpl implements ServicioCliente {

    @Autowired
    RepositorioCliente repositorioCliente;

    public final Map<String,Cliente> clientes = new ConcurrentHashMap<String,Cliente>();

    @Override
        public List<Cliente> obtenerClientes(int pagina, int cantidad) {
        final List<Cliente> clientesContados = List.copyOf(this.repositorioCliente.findAll());
        int indiceIncial = pagina * cantidad;
        int indiceFinal = indiceIncial + cantidad;

        System.out.println("=> Indice inicial : " + indiceIncial + " Indice final: " + indiceFinal);

        System.out.println((clientesContados.size()));

        if(indiceFinal > clientesContados.size()){
            indiceFinal = clientesContados.size();
        }
        System.out.println("Indice inicial : " + indiceIncial + "Indice final: " + indiceFinal + " " + clientesContados.size());
        return clientesContados.subList(indiceIncial, indiceFinal);
    }

    @Override
    public Cliente obtenerUnCliente(String idInterno) {
        final Optional<Cliente> quizasCliente = this.repositorioCliente.findById(idInterno);
        if (!quizasCliente.isPresent())
            throw new RuntimeException("No existe el cliente con documento " + idInterno);
        return quizasCliente.get();
    }

    @Override
    public void insertarClienteNuevo(Cliente cliente) {
        this.repositorioCliente.insert(cliente);
        System.err.println(String.format("==== insertarClienteNuevo %s = %s ","idInterno:", cliente.idInterno));
        System.err.println(String.format("==== insertarClienteNuevo %s = %s ","documento:", cliente.documento));
        System.err.println(String.format("==== insertarClienteNuevo %s = %s ","saldoPuntos:", cliente.saldoPuntos));
        System.err.println("************************************************");
    }

    @Autowired
    MongoOperations mongoOperations;

    @Override
    public void actualizarCliente(Cliente clientePar) {
        final Query query = query(where("_id").is(clientePar.idInterno));
        final Update update = new Update();

        //set(update,"idInterno",clientePar.idInterno);
        set(update,"documento",clientePar.documento );
        set(update,"saldoPuntos",clientePar.saldoPuntos);
        update.set("deudasConvenios", clientePar.deudasConvenios);
        update.set("codigosCanjes", clientePar.codigosCanjes);
        System.err.println("************************************************");
        mongoOperations.updateFirst(query,update,"cliente");
    }

    private void set(Update update, String nombre, Object valor) {
        if(valor != null) {
            System.err.println(String.format("*** actualizarCliente %s = %s", nombre, valor));
            update.set(nombre, valor);
        }
    }

    @Override
    public void borrarCliente(String idInterno) {
        this.repositorioCliente.deleteById(idInterno);
    }

    //Canjes
    @Autowired
    RepositorioCanje repositorioCanje;

    @Override
    public void agregarCanjeCliente(String idInterno, Canje canje) {
        final Cliente cliente = this.obtenerUnCliente(idInterno);

        System.err.println(String.format("==== agregarCanjeCliente %s = %s ","idInterno:", cliente.idInterno));
        System.err.println(String.format("==== agregarCanjeCliente %s = %s ","nroAutorizacion:", canje.nroAutorizacion));

        //if (!this.repositorioCanje.existsById(nroAutorizacion))
        //    throw new RuntimeException("Existe el código de autorización " + nroAutorizacion);

        //System.err.println("************************************************");
        /*cliente.codigosCanjes.add(canje.nroAutorizacion);
        this.repositorioCliente.save(cliente);
        this.repositorioCanje.insert(canje);*/

        System.err.println(String.format("==== agregarCanjeCliente %s = %s ","saldoPuntos:", cliente.saldoPuntos));
        System.err.println(String.format("==== agregarCanjeCliente %s = %s ","puntosCanjeados:", canje.puntosCanjeados));

        int newSaldoPuntos = cliente.saldoPuntos - canje.puntosCanjeados;
        if(newSaldoPuntos > 0) {
            cliente.saldoPuntos = newSaldoPuntos;
            canje.estado = "APROBADO";
            System.err.println(String.format("==== agregarCanjeCliente %s = %s ", "newSaldoPuntos:", newSaldoPuntos));
            cliente.codigosCanjes.add(canje.nroAutorizacion);
            this.repositorioCliente.save(cliente);
            this.repositorioCanje.insert(canje);
        }
        else {
            canje.estado = "RECHAZADO";
            cliente.codigosCanjes.add(canje.nroAutorizacion);
            this.repositorioCliente.save(cliente);
            this.repositorioCanje.insert(canje);
        }

        //***
    }

    @Override
    public CanjesCliente obtenerCanjesUnCliente(String idInterno) {
        final var r =
                this.mongoOperations.aggregate(newAggregation(
                        /* 1 */ match(where("_id").is(idInterno)),
                        /* 2 */ lookup("canje","codigosCanjes","_id","lstCanjes"),
                        /* 3 */ addFields().addFieldWithValue("idInterno", "$_id").build(),
                        /* 4 */ project("idInterno", "documento", "saldoPuntos", "lstCanjes")
                ), Cliente.class, CanjesCliente.class);
        final var canjesClientes = r.getMappedResults();
        if (canjesClientes.size() > 0)
            return canjesClientes.get(0);
        // Si no hay resultados -> objeto vacio.
        final var vacio = new CanjesCliente();
        vacio.idInterno = idInterno;
        return vacio;
    }
}
