package com.equipo04.pagoPuntos.servicio.impl;

import com.equipo04.pagoPuntos.modelo.DeudaConvenio;
import com.equipo04.pagoPuntos.modelo.Parametria;
import com.equipo04.pagoPuntos.modelo.Convenio;
import com.equipo04.pagoPuntos.servicio.ServicioConvenio;
import com.equipo04.pagoPuntos.servicio.ServicioDeudaConvenio;
import com.equipo04.pagoPuntos.servicio.ServicioParametria;
import com.equipo04.pagoPuntos.servicio.repositorio.RepositorioConvenio;
import com.equipo04.pagoPuntos.servicio.repositorio.RepositorioDeudaConvenio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ServicioDeudaConvenioImpl implements ServicioDeudaConvenio {

    @Autowired
    RepositorioDeudaConvenio repositorioDeudaConvenio;

    @Autowired
    ServicioParametria servicioParametria;

    @Autowired
    ServicioConvenio servicioConvenio;

    @Autowired
    RepositorioConvenio repositorioConvenio;

    @Override
    public void insertarDeudaConvenio(String codConvenio, DeudaConvenio deudaConvenio) {
        final Parametria parametria = this.servicioParametria.obtenerParametria("0001");
        final Convenio convenio = this.servicioConvenio.obtenerConvenio(codConvenio);

        if(parametria!=null){
            int factor = Integer.parseInt(parametria.valor);
            double importe  = deudaConvenio.importe;
            double equivalencia = factor * importe;
            System.out.println("equivalencia " + equivalencia);
            deudaConvenio.puntosEquivalentes = (int)(equivalencia);
            System.out.println("equivalencia " + equivalencia);
        }else{
            System.out.println("No existe la parametria. No se hara la conversion de puntos");
        }
        convenio.listaDeudas.add(deudaConvenio.codExterno);
        this.repositorioConvenio.save(convenio);
        this.repositorioDeudaConvenio.insert(deudaConvenio);
    }

    @Override
    public DeudaConvenio obtenerDeudaConvenio(String codExterno) {

        final Optional<DeudaConvenio> quizasDeudaConvenio = this.repositorioDeudaConvenio.findById(codExterno);
       /* if(quizasDeudaConvenio.isPresent()){
            final Parametria parametria = this.servicioParametria.obtenerParametria("0001");
            if(parametria!=null){
                int factor = Integer.parseInt(parametria.valor);
                int monto  = Integer.parseInt(quizasDeudaConvenio.get().importe);
                int equivalencia = factor*monto;
                quizasDeudaConvenio.get().puntosEquivalentes = equivalencia;
            }else{
                System.out.println("No existe el codigo de parametria");
            }
            return quizasDeudaConvenio.get();
        }else{
            return null;
        }*/
        return quizasDeudaConvenio.isPresent()? quizasDeudaConvenio.get():null;
    }

    @Override
    public Page<DeudaConvenio> listarDeudaConvenios(int pagina, int cantidad) {

        return this.repositorioDeudaConvenio.findAll(PageRequest.of(pagina,cantidad));
    }

    @Override
    public void actualizarDeudaConvenio(String codExterno, DeudaConvenio deudaConvenio) {
        deudaConvenio.codExterno = codExterno;
        this.repositorioDeudaConvenio.save(deudaConvenio);

    }

    @Override
    public void borrarDeudaConvenio(String codExterno) {
        this.repositorioDeudaConvenio.deleteById(codExterno);

    }
}
