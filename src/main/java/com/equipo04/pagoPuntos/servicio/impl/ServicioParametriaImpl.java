package com.equipo04.pagoPuntos.servicio.impl;

import com.equipo04.pagoPuntos.modelo.Parametria;
import com.equipo04.pagoPuntos.servicio.ServicioParametria;
import com.equipo04.pagoPuntos.servicio.repositorio.RepositorioParametria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ServicioParametriaImpl implements ServicioParametria {

    @Autowired
    RepositorioParametria repositorioParametria;

    @Override
    public void insertarParametria(Parametria parametria) {
        this.repositorioParametria.insert(parametria);
    }

    @Override
    public Parametria obtenerParametria(String codigo) {
        final Optional<Parametria> quizasParam = this.repositorioParametria.findById(codigo);
        return quizasParam.isPresent()? quizasParam.get():null;
    }

    @Override
    public Page<Parametria> listarParametria(int pagina, int cantidad) {
        return this.repositorioParametria.findAll(PageRequest.of(pagina,cantidad));
    }

    @Override
    public void actualizarParametria( String  codigo,Parametria parametria) {
        parametria.codigo = codigo;
        this.repositorioParametria.save(parametria);
    }

    @Override
    public void emparcharParametria(String codigo, Parametria parametria) {
        parametria.codigo = codigo;
        final Parametria miParam = this.obtenerParametria(codigo);

        if (miParam != null){
            if(miParam.descripcion!=null){
                miParam.descripcion= parametria.descripcion;
            }
            if(parametria.valor!=null){
                miParam.valor=parametria.valor;
            }

            this.actualizarParametria(codigo,parametria);
        }else{
            throw new RuntimeException("No existe el convenio: " + codigo);
        }
    }

    @Override
    public void borrarParametria(String codigo) {

        this.repositorioParametria.deleteById(codigo);
    }
}
