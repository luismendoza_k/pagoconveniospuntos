package com.equipo04.pagoPuntos.servicio.repositorio;

import com.equipo04.pagoPuntos.modelo.DeudaConvenio;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface RepositorioDeudaConvenio extends MongoRepository<DeudaConvenio, String> { }
