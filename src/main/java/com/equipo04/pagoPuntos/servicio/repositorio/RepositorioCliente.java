package com.equipo04.pagoPuntos.servicio.repositorio;

import com.equipo04.pagoPuntos.modelo.*;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface RepositorioCliente extends MongoRepository <Cliente,String> {

    //public void deleteByDocumento(String idInterno);

}
