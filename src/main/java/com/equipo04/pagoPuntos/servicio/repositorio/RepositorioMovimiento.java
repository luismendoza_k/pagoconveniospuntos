package com.equipo04.pagoPuntos.servicio.repositorio;

import com.equipo04.pagoPuntos.modelo.Movimiento;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface RepositorioMovimiento extends MongoRepository<Movimiento, String> { }
