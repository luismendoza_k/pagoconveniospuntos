package com.equipo04.pagoPuntos.servicio.repositorio;

import com.equipo04.pagoPuntos.modelo.Canje;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface RepositorioCanje extends MongoRepository<Canje, String> { }
