package com.equipo04.pagoPuntos.servicio.repositorio;

import com.equipo04.pagoPuntos.modelo.Parametria;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface RepositorioParametria extends MongoRepository<Parametria, String> { }