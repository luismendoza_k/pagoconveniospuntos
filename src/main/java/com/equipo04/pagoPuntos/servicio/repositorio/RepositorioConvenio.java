package com.equipo04.pagoPuntos.servicio.repositorio;

import com.equipo04.pagoPuntos.modelo.Convenio;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface RepositorioConvenio extends MongoRepository<Convenio,String> { }
