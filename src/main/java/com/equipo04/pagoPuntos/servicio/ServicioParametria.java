package com.equipo04.pagoPuntos.servicio;

import com.equipo04.pagoPuntos.modelo.Convenio;
import com.equipo04.pagoPuntos.modelo.Parametria;
import org.springframework.data.domain.Page;

public interface ServicioParametria {

    public void insertarParametria(Parametria parametria);

    public Parametria obtenerParametria(String codigo);

    public Page<Parametria> listarParametria(int pagina, int cantidad);

    public void actualizarParametria(String codigo,Parametria parametria);

    public void emparcharParametria(String codigo,Parametria parametria);

    public void borrarParametria(String codigo);

}
