package com.equipo04.pagoPuntos.servicio;

import com.equipo04.pagoPuntos.modelo.Convenio;
import com.equipo04.pagoPuntos.modelo.DeudaConvenio;
import org.springframework.data.domain.Page;

import java.util.List;

public interface ServicioDeudaConvenio {

    public void insertarDeudaConvenio(String codConvenio,DeudaConvenio deudaConvenio);

    public DeudaConvenio obtenerDeudaConvenio(String codExterno);

    public Page<DeudaConvenio> listarDeudaConvenios(int pagina, int cantidad);

    public void actualizarDeudaConvenio(String codExterno , DeudaConvenio deudaConvenio);

    public void borrarDeudaConvenio(String codConvenio);
}
