package com.equipo04.pagoPuntos.servicio;

import com.equipo04.pagoPuntos.modelo.Canje;
import com.equipo04.pagoPuntos.modelo.CanjesCliente;
import com.equipo04.pagoPuntos.modelo.Cliente;

import java.util.List;

public interface ServicioCliente {
    //CRUD
    public List<Cliente> obtenerClientes(int pagina, int cantidad);

    //READ
    public Cliente obtenerUnCliente(String idInterno);

    // CREATE
    public void insertarClienteNuevo(Cliente cliente);

    // UPDATE (solamente modificar, no crear).
    public void actualizarCliente(Cliente cliente);

    // DELETE
    public void borrarCliente(String idInterno);

    // Canjes
    public void agregarCanjeCliente(String idInterno, Canje canje);

    public CanjesCliente obtenerCanjesUnCliente(String idInterno);
}
