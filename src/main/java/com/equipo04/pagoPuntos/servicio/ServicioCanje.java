package com.equipo04.pagoPuntos.servicio;

import com.equipo04.pagoPuntos.modelo.Canje;
import com.equipo04.pagoPuntos.modelo.Cliente;

import java.util.List;

public interface ServicioCanje {

    public List<Canje> obtenerCanje();
    //public void insertarCanje(Canje canje);

}
