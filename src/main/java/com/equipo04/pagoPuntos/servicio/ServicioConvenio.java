package com.equipo04.pagoPuntos.servicio;

import com.equipo04.pagoPuntos.modelo.Convenio;
import com.equipo04.pagoPuntos.modelo.DeudaConvenio;
import org.springframework.data.domain.Page;

import java.util.List;

public interface ServicioConvenio {

    public void insertarConvenio(Convenio convenios);

    public Convenio obtenerConvenio(String codConvenio);

    public Page<Convenio> listarConvenios(int pagina, int cantidad);

    public void actualizarConvenio(String codConvenio,Convenio convenio);

    public void emparcharConvenio(String codConvenio,Convenio convenio);

    public void borrarConvenio(String codConvenio);

    public void agergarDeudaAConvenio(String codConvenio, DeudaConvenio deuda);

    public List<String> obtenerDeudasConvenio(String codConvenio);
}
