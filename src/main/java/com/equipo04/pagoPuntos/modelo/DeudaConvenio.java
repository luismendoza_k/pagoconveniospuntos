package com.equipo04.pagoPuntos.modelo;

import org.springframework.data.annotation.Id;

public class DeudaConvenio {

    @Id
    public String codExterno;
    public String recibo;
    public String fechaVencimiento;
    public String moneda;
    public Double importe;
    public String estado;
    public String fechaPago;
    public int puntosEquivalentes;
}
