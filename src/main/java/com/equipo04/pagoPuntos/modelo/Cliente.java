package com.equipo04.pagoPuntos.modelo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.annotation.Id;

import java.util.ArrayList;
import java.util.List;

public class Cliente {
    @Id public String idInterno;
    public String documento;
    public int saldoPuntos;

    @JsonIgnore
    public List<String> deudasConvenios = new ArrayList<>();

    @JsonIgnore
    public List<String> codigosCanjes = new ArrayList<>();

}
