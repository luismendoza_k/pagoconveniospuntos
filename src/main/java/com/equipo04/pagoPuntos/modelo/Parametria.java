package com.equipo04.pagoPuntos.modelo;

import org.springframework.data.annotation.Id;

public class Parametria {

    @Id
    public String codigo;
    public String descripcion;
    public String valor;

}
