package com.equipo04.pagoPuntos.modelo;

import org.springframework.data.annotation.Id;

public class Canje {

    @Id public String nroAutorizacion;
    public int puntosCanjeados;
    public Double importeCanje;
    public String moneda;
    public String estado;
    public String fechaCanje;
    public String nroMovimiento;

  //  public String idInterno;

}
