package com.equipo04.pagoPuntos.modelo;

import java.util.List;

public class MovimientosCliente {
    public String idInterno;
    public String documento;
    public String saldoPuntos;

    public List<Movimiento> lstDeudas;
}
