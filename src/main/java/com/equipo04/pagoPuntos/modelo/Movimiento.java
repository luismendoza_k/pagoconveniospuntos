package com.equipo04.pagoPuntos.modelo;

import org.springframework.data.annotation.Id;

public class Movimiento {

    @Id public String codExterno;
    public String nroMovimiento;
    public String tipoPago;
    public String nroAutorizacion;
    public String fecha;

}
