package com.equipo04.pagoPuntos.modelo;

import org.springframework.data.annotation.Id;

import java.util.ArrayList;
import java.util.List;

public class Convenio {

    @Id
    public String codConvenio;
    public String desConvenio;
    public String tipo;
    public String estado;

    public List<String> listaDeudas = new ArrayList<>();
}
